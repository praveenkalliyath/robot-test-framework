####********************************************************
##      ROBOT FRAMEWORK
####    Author: Praveen Kalliyath
####    Created Date: September 13 2020
####********************************************************

####    PIP Install Required
        pip install robotframework
        pip install robotframework-seleniumlibrary
        pip install arrow
        pip install Selenium
        pip install openpyxl
        pip install robotframework-faker
        pip install --upgrade robotframework-datadriver[XLS]
        pip install --upgrade robotframework-screencaplibrary
        pip install -U robotframework-pabot
        
####    Version Control
        Git
        GitLab

#####   Test Data Management
        Excel
        Json
        Faker
        Random Utils
        
####    Report
        Robot Frmaework Report
        
####    IDE
        Pycharm
        
 
