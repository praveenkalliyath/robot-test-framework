####********************************************************
##      ChangeLog
####********************************************************

#### Change-001
#####   Updated By: Praveen Kalliytah
#####   Date: September 12 2020
        Updated Folder Structures
        Created Common Settings Robot File
        Updated Output Saving Script To Custom Location
        Created Base keyword Robot File With Commonly Used Keywords
        Created Base Robot Files
         
#### Change-002
#####   Updated By: Praveen Kalliytah
#####   Date: September 13 2020
        Created Sample Test Cases
        Added Project to GITLAB
        Added Robot Data Faker
        Added Parallel Execution Using Pabot
        Added DataLibrary For Data Driven Testing
        Added ScreenCap Library For Video Recording
        
 
