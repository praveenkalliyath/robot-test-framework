*** Settings ***
#_____________________________________________________________________________________________________________________________
#Libraries
#_____________________________________________________________________________________________________________________________
Library     SeleniumLibrary      #  pip install robotframework-seleniumlibrary - Web testing library that uses popular Selenium tool internally.
Library     BuiltIn              #  Contains generic often needed keywords. Imported automatically and thus always available.
Library     OperatingSystem      #  Enables performing various operating system related tasks.
Library     Collections          #	Contains keywords for handling lists and dictionaries.
Library     DateTime             #  Supports creating and verifying date and time values as well as calculations between them
Library     Dialogs              #  Supports pausing the test execution and getting input from users
Library     Process              #  Supports executing processes in the system
Library     Screenshot           #  Provides keywords to capture and store screenshots of the desktop
Library     String               #  Library for manipulating strings and verifying their contents.
Library     XML                  #  Library for verifying and modifying XML documents
#Library     DataDriver           #  pip install --upgrade robotframework-datadriver[XLS] - Library for Data-Driven Testing with external 📤 data tables (csv, xls, xlsx, etc.).
#Library     FakerLibrary         #  pip install robotframework-faker - Library for Faker, a fake test data generator.
#Library     ScreenCapLibrary     #  pip install --upgrade robotframework-screencaplibrary - Library for taking screenshots and video recording. Similar functionality as Screenshot standard library, with some additional features.

#_____________________________________________________________________________________________________________________________
#Library      -   Python Files
#_____________________________________________________________________________________________________________________________
Library    ${EXECDIR}/Resources/PythonFiles/CommonUtil.py
Library    ${EXECDIR}/Resources/PythonFiles/ExcelUtil.py        #pip install openpyxl   -   Excel Utility For .xlsx, etc

#_____________________________________________________________________________________________________________________________
#Resources      -   Variables
#_____________________________________________________________________________________________________________________________
Resource    ${EXECDIR}/Resources/Variables/FrameworkVariables.robot
Resource    ${EXECDIR}/Resources/Variables/BaseVariables.robot
#Login
Resource    ${EXECDIR}/Resources/Variables/LoginPage.robot
#Dashboard
Resource    ${EXECDIR}/Resources/Variables/DashboardPage.robot


#_____________________________________________________________________________________________________________________________
#Resources      -   Keywords
#_____________________________________________________________________________________________________________________________
Resource    ${EXECDIR}/Resources/Keywords/BaseKeywords.robot
#Login
Resource    ${EXECDIR}/Resources/Keywords/LoginScreen.robot
#Dashboard
Resource    ${EXECDIR}/Resources/Keywords/Dashboard.robot



