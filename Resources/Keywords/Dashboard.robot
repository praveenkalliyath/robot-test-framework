*** Settings ***
Resource   ${EXECDIR}/Resources/Settings/BaseSettings.robot

*** Keywords ***

Get Page Title
    ${title}    Wait Until Element Contains     ${lbl_Dashboard}    Dashboard   ${Default_Timeout}
    [Return]    ${title}

Validate Dashboard Title
    Wait Until Keyword Succeeds     1200  5     Get Page Title