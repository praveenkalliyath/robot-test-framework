*** Settings ***
Resource    ${EXECDIR}/Resources/Settings/BaseSettings.robot


*** Keywords ***

Initialize
    [Documentation]  Keyword To Initialize The Test Setup
#    ${Date_Value}=  Get Current Date    result_format=${Default_Date_Format}
    Log Info   Setting Screenshot Folder To ${Screenshot_Folder}${Date_Value}
    Create Directory    ${OUTPUT DIR}${Screenshot_Folder}
    SeleniumLibrary.Set Screenshot Directory    ${OUTPUT DIR}${Screenshot_Folder}
#    ScreenCapLibrary.Set Screenshot Directory    ${OUTPUT DIR}${Screenshot_Folder}
#    Run Keyword If  '${Video_Recording}' in ['Yes','YES','Y','y']   Start Video Recording   name='Video_Record'
    Launch Application


Complete Execution
    [Documentation]     Keyword To Perform Teardown Methods
    Log Info  Result Folder is ${OUTPUT DIR}
#    Run Keyword If  '${Video_Recording}' in ['Yes','YES','Y','y']   Stop All Video Recordings
    Close All Browsers

Launch Application
    [Documentation]  Keyword To Launch Application Based On Selected Browser
#    [Arguments]     ${BROWSER}= "chrome"
    Log Info  Launching Browser And Navigating To Application
    Run Keyword If  ${BROWSER} == "chrome"  Open Browser    ${App_Url}  chrome
    ...     ELSE IF  ${BROWSER} == "edge"   Open Browser    ${App_Url}  edge
    ...     ELSE IF  ${BROWSER} == "ie"     Open Browser    ${App_Url}  ie
    ...     ELSE IF  ${BROWSER} == "firefox"    Open Browser    ${App_Url}  ff
    ...     ELSE    Log To Console   Please Pass Valid Browse Name. Browser provided was ${BROWSER}
    Maximize Browser Window
    Wait For Page Load

Start Driver And Open Application
    [Documentation]     Keyword To Start Driver And Login To The Application
    [Arguments]    ${BROWSER}    ${Driver_Path}
    Log Info  Browser is ${BROWSER}
    Log Info  Driver Path is ${Driver_Path}
    Create WebDriver    edge    executable_path='${Driver_Path}'
    Open Browser    ${App_Url}  edge   #executable_path=${Driver_Path}
    Maximize Browser Window

Wait For Page Load
    [Documentation]     Keyword To Wait For The Web Page To Load
    Wait For Condition	return document.readyState == 'complete'
    Wait For Condition	return jQuery.active == 0

Log Info
    [Documentation]     Keyword To Log Information To The Report
    [Arguments]     ${input}
    Log  ${input}   console=True    level=INFO    html=True

Log Warn
    [Documentation]     Keyword To Log Warning To The Report
    [Arguments]     ${input}
    Log  ${input}   console=True    level=WARN      html=True

Log Error
    [Documentation]     Keyword To Log Error To The Report
    [Arguments]     ${input}
    Log  ${input}   console=True    level=ERROR     html=True

Click
    [Documentation]     Keyword To Click On An Element After Element Is Visible
    [Arguments]     ${Element}
    Wait Until Element Is Visible   ${Element}  ${Default_Timeout}
    Run Keyword If  '${Take_Element_Screenshot}' in ['Yes','YES','Y','y']   Element Screenshot  ${Element}
    Click Element   ${Element}

Send Text
    [Documentation]     Keyword To Send Text To The Element
    [Arguments]     ${Element}      ${Input}
    Wait Until Element Is Visible   ${Element}  ${Default_Timeout}
    Input Text  ${Element}      ${Input}    clear=True

Wait For Element
    [Documentation]     Keyword To Wait For Element With Default Timeout
    [Arguments]     ${Element}
    Wait Until Element Is Visible   ${Element}  ${Default_Timeout}

Element Screenshot
    [Documentation]     Keyword To Take Element Screenshot And Save It To Folder
    [Arguments]     ${Element}  ${Filename}=Element
    Wait For Element    ${Element}
    ${DateTime}=  Get Current Date    result_format=${Default_DateTime_Format}
#    ${Date_Value}=  Get Current Date    result_format=${Default_Date_Format}
    Log Info   Image Location: ${OUTPUT DIR}${Screenshot_Folder}\\${Filename}_${DateTime}.png
    Capture Element Screenshot  ${Element}  ${Filename}_${DateTime}.png

Page Screenshot
    [Documentation]     Keyword To Take Page Screenshot And Save It To Folder
    [Arguments]     ${Filename}=Page
    Wait For Page Load
    ${DateTime}=  Get Current Date    result_format=${Default_DateTime_Format}
#    ${Date_Value}=  Get Current Date    result_format=${Default_Date_Format}
    Log Info  Image Location: ${OUTPUT DIR}${Screenshot_Folder}\\${Filename}_${DateTime}.png
    Capture Page Screenshot     ${Filename}_${DateTime}.png


#TODO LOOP AND NESTED FOR LOOP
Loop
    [Arguments]     ${Arg1}   ${Arg2}   ${Arg3}
    FOR  ${Arg1} IN RANGE ${Arg2}
        Run Keyword
        Log Info   I is ${i}
    END

Nested FOR Loop
    FOR	 ${x}	 IN 	RANGE 1 To 10
        Run Keyword  Loop
        Log Info   I is ${x}
    END
