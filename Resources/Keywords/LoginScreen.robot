*** Settings ***
Resource    ${EXECDIR}/Resources/Settings/BaseSettings.robot


*** Keywords ***
Enter Value For User Name
    Send Text   ${inp_UserEmail}    ${User_Email}

Enter Value For Password
    Send Text   ${inp_UserPassword}    ${User_Password}

Click On Login
    Click   ${btn_Login}

Login To ECommerce
    Page Screenshot     HomePage
    Enter Value For User Name
    Enter Value For Password
    Click On Login