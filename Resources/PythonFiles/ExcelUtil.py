'''
Created on 25-Jun-2020

@author: pk
'''
import openpyxl     #pip install openpyxl   -   Excel Utility

workbook = None
worksheet = None
excel_data_dictionary = {}


'''
Open Excel
@:param: file_path
@:param: sheet_name
'''
def Excel_Workbook(file_path,sheet_name):
    global workbook, worksheet
    print('Excel File: {0} Sheet Name: {1}'.format(file_path, sheet_name))
    workbook = openpyxl.load_workbook(file_path)
    worksheet = workbook[sheet_name]


'''
Get Row Count Used
@:return: row count used
'''
def Row_Count():
    return worksheet.max_row

'''
Get Column Count Used
@:return: column count used
'''
def Column_Count():
    return worksheet.max_column

'''
Get Column Number using Column Name
@:param: column  name
@:return: column number
'''
def Get_Column_Number_With_Value(column_name):
    index = 1;
    for c in range(1, (Column_Count() + 1)):
        if str(worksheet.cell(row=1, column=c).value) in (column_name):
            index = c
            break;
    return index
             
'''
Get Excel Row Records To Dictionary
@:param: column_name
@:return: dictionary
'''
def Get_Excel_Row_Data_To_Dictionary(column_value):
    global excel_data_dictionary
    excel_data_dictionary = {}
    col = Get_Column_Number_With_Value(column_value)
    for r in range(2, (Row_Count() + 1)):
        excel_data_dictionary[str(worksheet.cell(row=r, column=1).value)] = str(worksheet.cell(row=r, column=col).value) 
    print(excel_data_dictionary)
    return excel_data_dictionary

'''
Get Excel Column Records To Dictionary
@:param: row_number
@:return: dictionary
'''
def Get_Excel_Column_Data_To_Dictionary(row_number):
    global excel_data_dictionary
    excel_data_dictionary = {}
    for c in range(1, (Column_Count() + 1)):
        excel_data_dictionary[str(worksheet.cell(row=1, column=c).value)] = str(worksheet.cell(row=row_number, column=c).value) 
    print(excel_data_dictionary)
    return excel_data_dictionary



'''
Get Excel Column Records To Map
@:param: key
@:return: dictionary value
'''
def Excel_Key_Value(key):
    return excel_data_dictionary.get(key)


'''
Get Excel Row Records To Map
@:param: file_path
@:param: sheet_name 
@:param: column_name
@:return: dictionary 
'''
def Excel_Row_Hashmap(file_path,sheet_name,column_name):
    Excel_Workbook(file_path,sheet_name)
    return Get_Excel_Row_Data_To_Dictionary(column_name)

'''
Get Excel Column Records To Map
@:param: file_path
@:param: sheet_name 
@parm: row_number
@:return: dictionary 
'''
def Excel_Column_Hashmap(file_path,sheet_name,row_number):
    Excel_Workbook(file_path,sheet_name)
    return Get_Excel_Column_Data_To_Dictionary(row_number)

def sample_excel_test_function():
    Excel_Workbook('path to excel','Sheet1')
    print(Row_Count())
    print(Column_Count())
    print(Get_Column_Number_With_Value('Record-001'))
    print(Get_Excel_Row_Data_To_Dictionary('Record-001'))
    print(Excel_Key_Value('Country'))
     
    Excel_Workbook('path to excel','Sheet2')
    print(Row_Count())
    print(Column_Count())
    print(Get_Excel_Column_Data_To_Dictionary(2))
    print(Excel_Key_Value('Country'))

