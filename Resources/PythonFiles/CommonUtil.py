'''
Created on 12-September-2020
@author: Praveen Kalliyath
'''
import time
import random
import string
import math
import arrow    #pip install arrow - DateTime Utility
from datetime import datetime
from robot.libraries.BuiltIn import BuiltIn


DEFAULT_DATE_PATTERN = 'DD-MM-YYYY'
REFRESH_TIMEOUT = 5

'''
Generate Random Number with in range
@:param: minValue
@param maxValue
@:return: random number
'''
def Random_Number_With_In_Range(minValue, maxValue):
    return random.randrange(minValue, maxValue)


'''
Generate random number with specific length
@:param: length
@:return: random number
'''
def Random_Number(length):
    return ''.join(random.choices(string.digits, k=length))


'''
Generate random symbols with specific length
@:param: length
@:return: random symbols
'''
def Random_Symbols(length):
    return ''.join(random.choices(string.punctuation, k=length))


'''
Generate randomize a specific string
@:param: value
@:return: random text
'''
def Random_String(value):
    return ''.join(random.sample(value, k=len(value)))


'''
Generate random string with specific length
@:param: length
@:return: random string
'''
def Random_String_With_Letters_Digits_Symbols(length):
    return ''.join(random.choices(string.ascii_letters + string.digits + string.punctuation, k=length))


'''
Generate random alphabets with specific length
@:param: length
@:return: random alphabets
'''
def Random_Alphabets(length):
    return ''.join(random.choices(string.ascii_letters, k=length))


'''
Generate random alphanumeric value with specific length
@:param: lenghth
@:return: random alphanumeric
'''
def Random_Alphanumeric(length):
    return ''.join(random.choices(string.ascii_letters + string.digits, k=length))


'''
Generate random words with in range
@:param: word_count
@:return: random words
'''
def Random_Words(word_count):
    return {'sentence': (''.join(
        ''.join(random.choices(string.ascii_letters, k=Random_Number_With_In_Range(7, 20))) + ' ' for i in
        range(word_count)))}.get('sentence')


'''
Round number to specific digits
@:param: number
@:param: digits 
@:return: rounded number
'''
def Round_Number(number, digits):
    return round(number, digits)


'''
Round number to the largest integer
@:param: number
@:return: rounded number
'''
def Round_Number_To_The_Largest_Integer(number):
    return math.ceil(number)


'''
Round number to the smallest integer
@:param: number
@:return: rounded number
'''
def Round_Number_To_The_Smallest_Integer(number):
    return math.floor(number)


'''
Absolute number
@:param: number
@:return: absolute number
'''
def Absolute_Integer(number):
    return math.fabs(number)


'''
Modulus
@:param: x
@:param: y 
@:return: modulus number
'''
def Modulus(x, y):
    return round(math.fmod(x, y))


'''
Current date
@:return: todays date
'''
def Return_Todays_Date():
    return arrow.now()


'''
Current date in specified pattern
@:param: request_pattern 
@:return: todays date
'''
def Todays_Date_In_Pattern(request_pattern=DEFAULT_DATE_PATTERN):
    return arrow.now().format(request_pattern)


'''
Date in specified pattern
@:param: date
@:param: current_pattern
@:param: request_pattern 
@:return: todays date
'''
def Convert_Date_To_Pattern(date=Todays_Date_In_Pattern(), current_pattern=DEFAULT_DATE_PATTERN,
                            request_pattern='YYYY MMM, DD'):
    return arrow.get(date, current_pattern).format(request_pattern)


'''
Forward Date in specified pattern
@:param: days_count
@:param: request_pattern 
@:return: date
'''
def Return_Forward_Date(day_count, request_pattern=DEFAULT_DATE_PATTERN):
    return Return_Todays_Date().shift(days=+day_count).format(request_pattern)


'''
Backward Date in specified pattern
@:param: days_count
@:param: request_pattern 
@:return: date
'''
def Return_Backward_Date(day_count, request_pattern=DEFAULT_DATE_PATTERN):
    return Return_Todays_Date().shift(days=-day_count).format(request_pattern)


'''
Regex Pattern Validation
TODO
'''

'''
Convert map to json & vice versa
TODO
'''


'''
SELENIUM LIBRARY FUNCTIONS
'''

'''
GET BROWSER INSTANCE
@:return: browser instance
'''
def Browser():
    return  BuiltIn().get_library_instance('SeleniumLibrary')

'''
Get Element Using Locator Type And Value
@:param: locator
@return: web element
'''
def Element(locator):
    locator_type = locator[:locator.index(":")]
    locator_value = locator[(locator.index(":") + 1):]
    element = None
    if locator_type == "xpath":
        element = Browser().driver.find_element_by_xpath(locator_value)
    elif locator_type == "id":
        element = Browser().driver.find_element_by_id(locator_value)
    elif locator_type == "name":
        element = Browser().driver.find_element_by_name(locator_value)
    elif locator_type == "linktext":
        element = Browser().driver.find_element_by_link_text(locator_value)
    elif locator_type == "partial":
        element = Browser().driver.find_element_by_partial_link_text(locator_value)
    elif locator_type == "tag":
        element = Browser().driver.find_element_by_tag_name(locator_value)
    elif locator_type == "class":
        element = Browser().driver.find_element_by_class_name(locator_value)
    elif locator_type == "css":
        element = Browser().driver.find_element_by_css_selector(locator_value)
    else:
        print("Provide proper locator type")
    return element

'''
Perform Page Refresh Until Element Is Present
@:param: locator
@:return:   string
'''
def Refresh_Screen_Until_Element_Displayed(locator):
    displayed = "Not Found"
    element = Element(locator)
    while displayed == "Not Found":
        try:
            if element.is_displayed():
                    displayed = "Found"
        except:
            time.sleep(REFRESH_TIMEOUT)
            Browser().driver.refresh()

    return  displayed

'''
Get Dictionary Of Element Locator Type And Value
@:param: locator
'''
def Get_Locator(locator):
    locator_type = locator[:locator.index(":")]
    locator_value = locator[(locator.index(":") + 1):]
    dict_data = {'type':locator_type, 'value':locator_value}
    return dict_data



