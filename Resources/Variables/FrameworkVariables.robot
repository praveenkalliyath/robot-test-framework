*** Variables ***
${BROWSER}  'edge'
${App_Url}   https://admin-demo.nopcommerce.com/login

#_____________________________________________________________________________________________________________________________
#Driver Details
#_____________________________________________________________________________________________________________________________
${IE_Driver_Path}     '${EXECDIR}\\Resources\\Drivers\\IEDriverServer.exe'
${Edge_Driver_Path}     '${EXECDIR}\\Resources\\Drivers\\MicrosoftWebDriver.exe'
${Chrome_Driver_Path}     '${EXECDIR}\\Resources\\Drivers\\chromedriver.exe'
${Firefox_Driver_Path}     '${EXECDIR}\\Resources\\Drivers\\geckodriver.exe'

#_____________________________________________________________________________________________________________________________
#Default Values
#_____________________________________________________________________________________________________________________________
${Default_Timeout}  30
${Default_Date_Format}=  %d%m%Y
${Date_Value}
${Default_Time_Format}=  %H%M%S
${Time_Value}
${Default_DateTime_Format}=  %d%m%Y%H%M%S
${DateTime_Value}

${Take_Element_Screenshot}  YES
${Video_Recording}  Yes
#_____________________________________________________________________________________________________________________________
#Credentials
#_____________________________________________________________________________________________________________________________
${User_Email}    admin@yourstore.com
${User_Password}    admin

#_____________________________________________________________________________________________________________________________
#Test Data Folders
#_____________________________________________________________________________________________________________________________
${Resources_Folder}      ${EXECDIR}\\Resources\\
${TestData_Folder}       ${Resources_Folder}\\TestData\\
${ExcelFiles_Folder}     ${TestData_Folder}Excels\\
${JsonsFiles_Folder}     ${TestData_Folder}Jsons\\
${TextFiles_Folder}      ${TestData_Folder}Texts\\
${FlatFiles_Folder}      ${TestData_Folder}FlatFiles\\

#_____________________________________________________________________________________________________________________________
#Report Folders
#_____________________________________________________________________________________________________________________________
${Results_Folder}       ${EXECDIR}\\Results\\
${Screenshot_Folder}    Screenshots\\