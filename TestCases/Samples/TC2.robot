*** Settings ***
Resource    ${EXECDIR}/Resources/Settings/BaseSettings.robot

Suite Setup  Initialize
Suite Teardown   Complete Execution

*** Test Cases ***
Valdiate Login Functionality For ECommerce Site
    [Tags]  Regression
    Login To ECommerce
    Validate Dashboard Title



#----------------------------------------------------------------------
# Commands To Run Robot Test Cases
#----------------------------------------------------------------------
# Parallel Running  - --processes= mention the noumber of testcases to be executed parallely
# pabot --processes 3 path/*.robot.

# Saving Output to Different Folder
# robot --outputdir=Results path\*.robot
# pabot --processes 3 --outputdir=Results path/*.robot.

# Running Using Tags
# robot --include=tagName path\*.robot

# Running Using Multiple Tags   : i = include
# robot -i tagName1 -i tagName2 path\*.robot

# Running Using Multiple Tags : i = include; e = exclude
# robot -i tagName1 -e tagName2 path\*.robot

# Running Using Exclude Tags : e = exclude
# robot -e tagName2 path\*.robot
# execute test cases without tag "two" in any file.
# robot --exclude two path\*.robot

# Running Single TestCase Files
# robot path\TC1.robot

# Running Multiple TestCase Files
# robot path\TC1.robot,path\TC2.robot

# Running Specific TestCase Inside A File
# robot -t "testcase keyword" path\*.robot
# robot -t "testcase keyword" path\filename.robot

# execute test cases with name "Example" in any file.
# robot --test Example

# execute test cases with name "Example" in specific file.
# robot --test Example example.robot

# execute test cases from suites named "positive" in any file.
# robot --suite positive

# execute test cases from suite "feature1\positive" in any file.
# robot --suite feature1.positive

# execute test cases failed in previous run (saved in output.xml)
# robot --rerunfailed output.xml

# execute test cases with failed test cases in previous run (saved in output.xml)
# robot --rerunfailedsuites output.xml

# execute test cases containing name "Example" in any file.
# robot --test *Example*

# execute test cases "Example One" and "Example Two" in any file.
# robot --test "Example [One|Two]"

# execute test cases with tags starting with "One" in any file.
# robot --include One*

# execute test cases without tags ending with "Two" in any file.
# robot --exclude *Two

# execute test cases from suites starting with "positive" in any file.
# robot --suite positive*

# execute test cases containing name "Example" and having tag "One" in any file.
# robot --include One --test *Example*

# execute test cases from suite "FeatureA" exluding tests with tag "Smoke" in any file.
# robot --suite FeatureA --exclude Smoke

# execute test cases with tag "Pending" from specific file.
# robot --exclude Pending example.robot


#If you're worried about the execution order of the test cases/suites, you can prepend a prefix to the names. For example:
#01__a_suite
#|_____ 01__some_tests.txt
#|_____ 02__more_tests.txt
#
#02__another_suite
#|_____ 01__some_tests.txt
#|_____ 02__more_tests.txt
#----------------------------------------------------------------------
