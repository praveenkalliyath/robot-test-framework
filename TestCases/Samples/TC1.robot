*** Settings ***
Resource       ${EXECDIR}/Resources/Settings/BaseSettings.robot

*** Test Cases ***
Generate Random Numbers
    [Tags]  random
    ${length}=   convert to integer     5
    ${value}=    CommonUtil.Random Number   ${length}
    Log Info  Random Number: ${value}
    ${value}=    CommonUtil.Random Alphabets   ${length}
    Log Info  Random Alpabets: ${value}
    ${value}=    CommonUtil.Random Alphanumeric   ${length}
    Log Info  Random Alpabets And Numbers: ${value}

Perform Date Operations
    [Tags]  date
    ${date}=  CommonUtil.Convert Date To Pattern    request_pattern='YYYY MMM, DD'
    Log Info  Converted Date Pattern is: ${date}

Perform Excel Actions Row Wise
    [Tags]  excel
    &{Data_Dictionary}=   ExcelUtil.Excel Row Hashmap   ${EXECDIR}\\Resources\\TestData\\Excels\\DataBook.xlsx   Sheet1    Record-001
    Log Info  Excel Dictionary Data: ${Data_Dictionary}
    ${email}=    ExcelUtil.Excel Key Value   User Email
    Log Info  Email: ${email}
    ${password}=    Get From Dictionary     ${Data_Dictionary}     Password
    Log Info  Password: ${password}


Perform Excel Actions Column Wise
    [Tags]  excel
    ${row_number}=  convert to integer     2
    &{Data_Dictionary}=   ExcelUtil.Excel Column Hashmap   ${EXECDIR}\\Resources\\TestData\\Excels\\DataBook.xlsx   Sheet1     ${row_number}
    Log Info  Excel Dictionary Data: ${Data_Dictionary}
    ${email}=    ExcelUtil.Excel Key Value   FieldName
    Log Info  Email: ${email}
    ${password}=    Get From Dictionary     ${Data_Dictionary}     Record-001
    Log Info  Password: ${password}