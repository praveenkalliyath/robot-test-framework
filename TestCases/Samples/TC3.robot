*** Settings ***
Resource    ${EXECDIR}/Resources/Settings/BaseSettings.robot

Test Setup  Initialize
Test Teardown   Complete Execution

*** Test Cases ***
Perform While Loop
    Log Info  Refresh Scenario
    &{Data}=  CommonUtil.Get Locator  xpath://input[@value='Log in']
    Log Info  Locator Data &{Data}
    ${found}=  CommonUtil.Refresh Screen Until Element Displayed   xpath://input[@value='Log in']
    Log Info  Element ${found}